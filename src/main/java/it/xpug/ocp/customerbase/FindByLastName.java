package it.xpug.ocp.customerbase;

import java.util.ArrayList;
import java.util.List;

public class FindByLastName implements IFinder{

	@Override
	public List<Customer> findCustomer(Customer customer, List<Customer> customers) {
		List<Customer> result = new ArrayList<Customer>();
		for (Customer iteratorCustomer : customers) {
			if (iteratorCustomer.lastName().equals(customer.lastName())) {
				result.add(iteratorCustomer);
			}
		}
		return result;
	}
}
