package it.xpug.ocp.customerbase;

import java.util.ArrayList;
import java.util.List;

public class FindByCreditGreaterThan implements IFinder{

	@Override
	public List<Customer> findCustomer(Customer customer, List<Customer> customers) {
		List<Customer> result = new ArrayList<Customer>();
		for (Customer iteratorCustomer : customers) {
			if (iteratorCustomer.credit() > customer.credit()) {
				result.add(iteratorCustomer);
			}
		}
		return result;
	}

}
