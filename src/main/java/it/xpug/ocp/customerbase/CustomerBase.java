package it.xpug.ocp.customerbase;
import java.util.*;


public class CustomerBase {

	private List<Customer> customers = new ArrayList<Customer>();

	public void add(Customer customer) {
		customers.add(customer);
	}
	
	private IFinder selectTypeFinder(int option) {
		IFinder result= null;
		if (option==1) {
			result = new FindByLastName();
		}
		if (option==2) {
			result = new FindByFirstAndLastName();
		}
		if (option==3) {
			result = new FindByCreditGreaterThan();
		}
		return result;
	}
	
	public List<Customer> findCustomers(Customer customer, int option){
		List<Customer> listResult = null;
		listResult = selectTypeFinder(option).findCustomer(customer, customers);
		return listResult;
	}

}
