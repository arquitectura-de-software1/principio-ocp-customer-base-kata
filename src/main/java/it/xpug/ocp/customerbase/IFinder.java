package it.xpug.ocp.customerbase;

import java.util.List;

public interface IFinder {
	List<Customer> findCustomer(Customer customer, List<Customer> customers);
}
