package it.xpug.ocp.customerbase;

import java.util.ArrayList;
import java.util.List;

public class FindByFirstAndLastName implements IFinder{

	@Override
	public List<Customer> findCustomer(Customer customer, List<Customer> customers) {
		List<Customer> result = new ArrayList<Customer>();
		for (Customer iteratorCustomer : customers) {
			if (iteratorCustomer.firstName().equals(customer.firstName()) && iteratorCustomer.lastName().equals(customer.lastName()) ) {
				result.add(iteratorCustomer);
			}
		}
		return result;
	}

}
